import 'package:flutter/material.dart';

/// FileName main_page
///
/// @Author 王冲
/// @Date 2021/12/11 22:09
///
/// @Description TODO

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.green
      ),
    );
  }
}
