import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/flutter_boost.dart';
import 'package:flutter_boost_redux_demo/core/router/config.dart';
import 'package:flutter_boost_redux_demo/core/router/lifecycle_test_page.dart';
import 'package:flutter_boost_redux_demo/ui/main_page.dart';

/// FileName flutter_boost_router
///
/// @Author 王冲
/// @Date 2021/12/11 22:04
///
/// @Description

class FlutterBoostRouter {

  /// 由于很多同学说没有跳转动画，这里是因为之前exmaple里面用的是 [PageRouteBuilder]，
  /// 其实这里是可以自定义的，和Boost没太多关系，比如我想用类似iOS平台的动画，
  /// 那么只需要像下面这样写成 [CupertinoPageRoute] 即可
  /// (这里全写成[MaterialPageRoute]也行，这里只不过用[CupertinoPageRoute]举例子)
  ///
  /// 注意，如果需要push的时候，两个页面都需要动的话，
  /// （就是像iOS native那样，在push的时候，前面一个页面也会向左推一段距离）
  /// 那么前后两个页面都必须是遵循CupertinoRouteTransitionMixin的路由
  /// 简单来说，就两个页面都是CupertinoPageRoute就好
  /// 如果用MaterialPageRoute的话同理

  static Map<String, FlutterBoostRouteFactory> _routerMap = {
    'mainPage': (settings, uniqueId) {
      return CupertinoPageRoute(
          settings: settings,
          builder: (_) {
            Map<String, Object> map = settings.arguments ?? {};
            String data = map['data'] ?? '';
            return MainPage(
            );
          });
    },
    ///生命周期例子页面
    'lifecyclePage': (settings, uniqueId) {
      return CupertinoPageRoute(
          settings: settings,
          builder: (ctx) {
            return LifecycleTestPage();
          });
    }
  };

  static Route<dynamic> routeFactory(RouteSettings settings, String uniqueId) {
    FlutterBoostRouteFactory func = _routerMap[settings.name];
    if (func == null) {
      if(ConfigManger.instance.isFromNative) {
        return null;
      } else {
        func = _routerMap['mainPage'];
      }
    }
    return func(settings, uniqueId);
  }
}