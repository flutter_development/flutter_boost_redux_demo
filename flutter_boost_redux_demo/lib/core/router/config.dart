/// FileName config
///
/// @Author 王冲
/// @Date 2021/12/11 22:39
///
/// @Description

class ConfigManger {
  // 工厂方法构造函数
  factory ConfigManger() => _getInstance();

  // instance的getter方法，singletonManager.instance获取对象
  static ConfigManger get instance => _getInstance();

  // 静态变量_instance，存储唯一对象
  static ConfigManger _instance = null;

  // 获取对象
  static ConfigManger _getInstance() {
    if (_instance == null) {
      // 使用私有的构造方法来创建对象
      _instance = ConfigManger._internal();
    }
    return _instance;
  }

  // 私有的命名式构造方法，通过它实现一个类 可以有多个构造函数，
  // 子类不能继承internal
  // 不是关键字，可定义其他名字
  ConfigManger._internal() {
    //初始化...

  }

  Map<String, dynamic> headersMap;
  /// 是否是flutter单独运行的测试
  bool isFromNative = false;
}